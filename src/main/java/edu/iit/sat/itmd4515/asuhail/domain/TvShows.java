/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.asuhail.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author Abdullah
 */
@Entity
@Table(name = "tvShows")
@NamedQuery(
        name = "TvShows.findByName",
        query = "select t from TvShows t where t.showName = :showName")
public class TvShows {

    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "showId", unique = true)
    private Long showId;
    @Column(name = "showName", length = 14)
    private String showName;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "showAirdate")
    private Date showAirdate;

    public TvShows(Long showId, String showName, Date showAirdate) {
        this.showId = showId;
        this.showName = showName;
        this.showAirdate = showAirdate;
    }

    public TvShows(String showName, Date showAirdate) {
        this.showName = showName;
        this.showAirdate = showAirdate;
    }

    /**
     * Get the value of showAirdate
     *
     * @return the value of showAirdate
     */
    public Date getShowAirdate() {
        return showAirdate;
    }

    /**
     * Set the value of showAirdate
     *
     * @param showAirdate new value of showAirdate
     */
    public void setShowAirdate(Date showAirdate) {
        this.showAirdate = showAirdate;
    }

    
    /**
     * Get the value of showId
     *
     * @return the value of showId
     */
    public Long getShowId() {
        return showId;
    }

    /**
     * Set the value of showId
     *
     * @param showId new value of showId
     */
    public void setShowId(Long showId) {
        this.showId = showId;
    }


    /**
     * Get the value of showName
     *
     * @return the value of showName
     */
    public String getShowName() {
        return showName;
    }

    /**
     * Set the value of showName
     *
     * @param showName new value of showName
     */
    public void setShowName(String showName) {
        this.showName = showName;
    }

    @Override
    public String toString() {
        return "TvShows{" + "showId=" + showId + ", showName=" + showName + ", showAirdate=" + showAirdate + '}';
    }

    public TvShows() {
    }

}
