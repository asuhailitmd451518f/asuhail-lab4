/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.asuhail.domain;

import java.io.OptionalDataException;
import java.util.GregorianCalendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.RollbackException;
import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Abdullah
 */
public class TvShowsTest {

    private static EntityManagerFactory emf;
    private EntityManager em;
    private EntityTransaction et;

    @BeforeClass
    public static void beforeClass() {
        emf = Persistence.createEntityManagerFactory("itmd4515PU");
    }

    @AfterClass
    public static void afterClass() {
        emf.close();
    }

    @Before
    public void beforeTest() {
        em = emf.createEntityManager();
        et = em.getTransaction();

        TvShows seed = new TvShows(
                "SEED",
                new GregorianCalendar(2018, 1, 1).getTime());

        et.begin();
        em.persist(seed);
        et.commit();
    }

    @After
    public void afterTest() {

        TvShows seed = em
                .createNamedQuery("TvShows.findByName", TvShows.class)
                .setParameter("showName", "SEED")
                .getSingleResult();

        et.begin();
        em.remove(seed);
        et.commit();

        if (em != null) {
            em.close();
        }
    }

    @Test
    public void persistNewTvShowSuccess() {
        TvShows show1;
        show1 = new TvShows("Modern Family",
                new GregorianCalendar(2009, 9, 23).getTime());

        et.begin();
        em.persist(show1);
        et.commit();
    }

    @Test(expected = RollbackException.class)
    public void persistNewTvShowFailure() {
        TvShows show1;
        show1 = new TvShows("The Adventures of Jimmy Neutron: Boy Genius",
                new GregorianCalendar(2002, 7, 20).getTime());

        et.begin();
        em.persist(show1);
        et.commit();
    }

    @Test
    public void readNewTvShowSuccess() {
        et.begin();
        System.out.println(em.find(TvShows.class, 3l));
        et.commit();
    }

    @Test
    public void readNewTvShowFailure() {

        et.begin();
        System.out.println(em.find(TvShows.class, 1l));
        et.commit();
    }

    @Test
    public void removeNewTvShowSuccess() {
        TvShows show1;
        show1 = new TvShows("Modern Family",
                new GregorianCalendar(2009, 9, 23).getTime());

        et.begin();
        em.remove(show1);
        et.commit();
    }

    @Test
    public void removeNewTvShowFailure() {
        TvShows show1;
        show1 = new TvShows("Modern Family",
                new GregorianCalendar(2009, 9, 23).getTime());

        et.begin();
        em.remove(show1);
        et.commit();
    }
    
        @Test
    public void updateNewTvShowSuccess() {
        TvShows show1;
        show1 = new TvShows("Modern Family",
                new GregorianCalendar(2009, 9, 23).getTime());

        et.begin();
        em.merge(show1);
        et.commit();
    }

    @Test
    public void updateNewTvShowFailure() {
        TvShows show1;
        show1 = new TvShows("Modern Family",
                new GregorianCalendar(2009, 9, 23).getTime());

        et.begin();
        em.merge(show1);
        et.commit();
    }
}
